<?php
declare(strict_types=1);
namespace Nakima\Utils\Server;
use Nakima\CoreBundle\Utils\Symfony;

/**
 * @author xgonzalez@nakima.es
 */

class Client
{

    /**
     * @return string
     */
    public static function getIp(): string
    {
        $env = Symfony::getContainer()->getParameter('kernel.environment');
        if ($env === "test") {
            return "127.0.0.1";
        }
        return $_SERVER["HTTP_X_REAL_IP"] ?? $_SERVER["REMOTE_ADDR"] ?? "0.0.0.0";
    }

    /**
     * @return mixed
     */
    public static function getUserAgent()
    {
        return $_SERVER["HTTP_USER_AGENT"];
    }

    /**
     * @return null
     */
    public static function getCountryCode()
    {
        if (function_exists("geoip_open")) {
            $gi = geoip_open(getcwd()."/../cities.dat", GEOIP_STANDARD);
            $geo = geoip_record_by_addr($gi, self::getIp());
            geoip_close($gi);

            if ($geo) {
                return $geo->country_code;
            }
        } else {
            $geo = geoip_record_by_name(self::getIp());
            if ($geo) {
                return $geo["country_code"];
            }
        }

        return null;
    }

    /**
     * @return mixed
     */
    public static function getCity()
    {
        if (function_exists("geoip_open")) {
            $gi = geoip_open(getcwd()."/../cities.dat", GEOIP_STANDARD);
            $geo = geoip_record_by_addr($gi, self::getIp());
            geoip_close($gi);

            if ($geo) {
                return $geo->city;
            }
        } else {
            $geo = geoip_record_by_name(self::getIp());
            if ($geo) {
                return $geo["city"];
            }
        }
    }


    /**
     * @param $userAgent
     * @return array
     */
    private static function parseUserAgent($userAgent)
    {

        $ret = array();

        if (preg_match('/(Android [.0-9]+)/ism', $userAgent, $out)) {
            $ret["platform"] = $out[1];
        } else {
            if (preg_match('/Android/ism', $userAgent, $out)) {
                $ret["platform"] = "Android";
            } else {
                if (preg_match('/iPad/ism', $userAgent, $out)) {
                    $ret["platform"] = "iPad";
                } else {
                    if (preg_match('/iPhone/ism', $userAgent, $out)) {
                        $ret["platform"] = "iPhone";
                    } else {
                        if (preg_match('/iPod touch/ism', $userAgent, $out)) {
                            $ret["platform"] = "iPod touch";
                        } else {
                            if (preg_match('/iPod/ism', $userAgent, $out)) {
                                $ret["platform"] = "iPod";
                            } else {
                                if (preg_match('/Linux x86_64/ism', $userAgent, $out)) {
                                    $ret["platform"] = "Linux x86_64";
                                } else {
                                    if (preg_match('/Linux x86_64/ism', $userAgent, $out)) {
                                        $ret["platform"] = "Linux x86_64";
                                    } else {
                                        if (preg_match('/IEMobile/ism', $userAgent, $out)) {
                                            $ret["platform"] = "IEMobile";
                                        } else {
                                            if (preg_match('/Win64/ism', $userAgent, $out)) {
                                                $ret["platform"] = "Win64";
                                            } else {
                                                if (preg_match('/Windows NT/ism', $userAgent, $out)) {
                                                    $ret["platform"] = "Win32";
                                                } else {
                                                    if (preg_match('/Win32/ism', $userAgent, $out)) {
                                                        $ret["platform"] = "Win32";
                                                    } else {
                                                        if (preg_match('/Windows NT/ism', $userAgent, $out)) {
                                                            $ret["platform"] = "Win";
                                                        } else {
                                                            if (preg_match('/Linux i686/ism', $userAgent, $out)) {
                                                                $ret["platform"] = "Linux i686";
                                                            } else {
                                                                if (preg_match(
                                                                    '/PlayStation 4/ism',
                                                                    $userAgent,
                                                                    $out
                                                                )) {
                                                                    $ret["platform"] = "PlayStation 4";
                                                                } else {
                                                                    if (preg_match(
                                                                        '/Macintosh/ism',
                                                                        $userAgent,
                                                                        $out
                                                                    )) {
                                                                        $ret["platform"] = "Macintosh";
                                                                    } else {
                                                                        $ret["platform"] = "";
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $ret["version"] = "";
        $ret["browser"] = "";
        $ret["adblock"] = true;
        $ret["bot"] = true;

        $versionGroup = array(
            "Edge",
            "Chromium",
            "Epiphany",
            "MiuiBrowser",
            "SkypeUriPreview Preview",
            "FBAV",
            "Firefox",
        );

        $versionGroupSpaced = array(
            "DAUM",
            "PlayStation 4",
        );

        $noVersionGroup = array(
            "Opera Mini",
            "Twitter for iPhone",

        );

        $versionBotGroup = array(
            "curl",
            "Digg Deeper",
            "Google-HTTP-Java-Client",
            "AhrefsBot",
            "Baiduspider",
            "bingbot",
            "BingPreview",
            "Googlebot",
            "facebookexternalhit",
            "SeznamBot",
            "Applebot",
            "zgrab",
            "TelegramBot",
            "Twitterbot",
            "Twitter",
            "wget",
            "DotBot",
            "WhatsApp",
            "python-requests",
            "LivelapBot",
            "MetaURI API",
            "GrapeshotCrawler",
            "msnbot-media",
            "Nutch",

        );

        $noVersionBotGroup = array(
            "Mediapartners-Google",
            "NewShareCounts.com",
            "Yahoo! Slurp",
            "WhatsApp",
            "nlpproject",
            "apache",
            "Indy Library",
            "semantic-visions.com",
            "proximic",

        );

        $versionBotGroupV = array(
            "MJ12bot",
        );

        $found = false;

        // SPecials
        if (preg_match('/MSIE ([.0-9]+)/ism', $userAgent, $out)) {
            $ret["browser"] = "Internet Explorer";
            $ret["version"] = $out[1];
            $ret["adblock"] = true;
            $ret["bot"] = false;
            $found = true;
        }

        if (!$found) {
            foreach ($versionGroup as $key => $value) {
                if (preg_match("/$value\/([.0-9]+)/ism", $userAgent, $out)) {
                    $ret["browser"] = $value;
                    $ret["version"] = $out[1];
                    $ret["bot"] = false;
                    $found = true;
                }
            }
        }

        if (!$found) {
            foreach ($versionGroupSpaced as $key => $value) {
                if (preg_match("/$value ([.0-9]+)/ism", $userAgent, $out)) {
                    $ret["browser"] = $value;
                    $ret["version"] = $out[1];
                    $ret["bot"] = false;
                    $found = true;
                }
            }
        }

        if (!$found) {
            foreach ($versionBotGroup as $key => $value) {
                if (preg_match("/$value\/([.0-9]+)/ism", $userAgent, $out)) {
                    $ret["browser"] = "[BOT]".$value;
                    $ret["version"] = $out[1];
                    $ret["bot"] = true;
                    $found = true;
                }
            }
        }

        if (!$found) {
            foreach ($versionBotGroupV as $key => $value) {
                if (preg_match("/$value\/v([.0-9]+)/ism", $userAgent, $out)) {
                    $ret["browser"] = "[BOT]".$value;
                    $ret["version"] = $out[1];
                    $ret["bot"] = true;
                    $found = true;
                }
            }
        }

        if (!$found) {
            foreach ($noVersionGroup as $key => $value) {
                if (preg_match("/$value/ism", $userAgent, $out)) {
                    $ret["browser"] = $value;
                    $ret["version"] = "";
                    $ret["bot"] = false;
                    $found = true;
                }
            }
        }

        if (!$found) {
            foreach ($noVersionBotGroup as $key => $value) {
                if (preg_match("/$value/ism", $userAgent, $out)) {
                    $ret["browser"] = "[BOT]".$value;
                    $ret["version"] = "";
                    $ret["bot"] = true;
                    $found = true;
                }
            }
        }

        if (!$found) {
            if ($userAgent == "Mozilla") {
                $ret["browser"] = "Mozilla";
                $ret["bot"] = true;
                $found = true;
            } else {
                if (preg_match('/Chrome\/([.0-9]+)/ism', $userAgent, $out)) {
                    $ret["browser"] = "Chrome";
                    $ret["version"] = $out[1];
                    $ret["bot"] = false;
                    $found = true;
                } else {
                    if (preg_match('/Mobile\/13C75/ism', $userAgent, $out)) {
                        $ret["browser"] = "Safari";
                        $ret["bot"] = false;
                        $found = true;
                    } else {
                        if (preg_match('/Safari/ism', $userAgent, $out)) {
                            $ret["browser"] = "Safari";
                            $ret["bot"] = false;
                            $found = true;
                        }
                    }
                }
            }
        }

        if (preg_match('/Version\/([.0-9]+)/ism', $userAgent, $out)) {
            $ret["version"] = $out[1];
        }

        $ret["ok"] = $found;

        return $ret;
    }
}
