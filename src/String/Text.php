<?php
declare(strict_types=1);

namespace Nakima\Utils\String;

class Text
{

    const UPPER_LETTERS = "ABCDFGHIJKLMNOPQRSTUVWXYZ";

    public static function slugify(string $string, array $replace = [], string $delimiter = '_'): string
    {
        $clean = $string;
        if (!empty($replace)) {
            $clean = str_replace((array)$replace, ' ', $clean);
        }

        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower($clean);
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
        $clean = trim($clean, $delimiter);

        return $clean;
    }

    public static function rstr(int $length = 8): string
    {
        $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $count = strlen($chars);

        $bytes = random_bytes($length);
        $result = '';
        foreach (str_split($bytes) as $byte) {
            $result .= $chars[ord($byte) % $count];
        }

        return $result;
    }

    public static function rstr2(int $length = 8): string
    {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $count = strlen($chars);

        $bytes = random_bytes($length);
        $result = '';
        foreach (str_split($bytes) as $byte) {
            $result .= $chars[ord($byte) % $count];
        }

        return ucfirst($result);
    }

    public static function startsWith(string $haystack, string $needle): bool
    {
        return $needle === "" || strpos($haystack, $needle) === 0;
    }

    public static function nextUpperLetter(string $letter): string
    {
        $i = strlen($letter) - 1;

        do {
            $carry = false;

            $carry = $letter[$i] == "Z";

            if ($carry) {
                $letter[$i] = "A";
            } else {
                $letter[$i] = self::UPPER_LETTERS[strpos(self::UPPER_LETTERS, $letter[$i]) + 1];
            }

            $i--;
        } while ($carry && $i >= 0);

        if ($carry) {
            $letter = "A".$letter;
        }

        return $letter;
    }

    public static function validateEmail(string $email): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

    public static function validateName(string $name): bool
    {
        return preg_match('/^[\p{L} \s-]+$/u', $name) > 0;
    }

    public static function getPasswordStrength($hash)
    {

        $keys = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#\/-_$%^&+=!¿?()[]{},.;:';

        for ($i = 0; $i < strlen($hash); $i++) {
            if (strpos($keys, $hash[$i]) === false) {
                return -1;
            }
        }

        $numbers = false;
        $lower = false;
        $upper = false;
        $symbols = false;

        $numbers = preg_match('/.*[0-9].*/', $hash);
        $lower = preg_match('/.*[a-z].*/', $hash);
        $upper = preg_match('/.*[A-Z].*/', $hash);
        $symbols = preg_match('/.*[@#\-_$%^&+=!¿\?\(\)\[\]{}*\/,.:;].*/', $hash);


        if ($numbers && $lower && $upper && $symbols) {
            return 3;
        }

        if ($numbers && $lower && $upper) {
            return 2;
        }

        if ($numbers || $lower || $upper) {
            return 1;
        }

        return 0;
    }
}
