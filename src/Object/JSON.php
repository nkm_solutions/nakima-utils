<?php
declare(strict_types=1);
namespace Nakima\Utils\Object;

/**
 * @author xgonzalez@nakima.es
 */

class JSON
{

    public static function decode($object, $b = true)
    {
        return json_decode($object, $b);
    }

    public static function encode($object)
    {
        return json_encode($object);
    }

    public static function lastError()
    {
        return json_last_error();
    }

    public static function lastErrorMessage()
    {
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return false;
            case JSON_ERROR_DEPTH:
                return "JSON_ERROR_DEPTH";
            case JSON_ERROR_STATE_MISMATCH:
                return "JSON_ERROR_STATE_MISMATCH";
            case JSON_ERROR_CTRL_CHAR:
                return "JSON_ERROR_CTRL_CHAR";
            case JSON_ERROR_SYNTAX:
                return "JSON_ERROR_SYNTAX";
            case JSON_ERROR_UTF8:
                return "JSON_ERROR_UTF8";
            default:
                return 'Unknown json error';
        }
    }

    public static function throwLastError()
    {
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                break;
            case JSON_ERROR_DEPTH:
                throw new \Exception('Maximum stack depth exceeded');
            case JSON_ERROR_STATE_MISMATCH:
                throw new \Exception('Underflow or the modes mismatch');
            case JSON_ERROR_CTRL_CHAR:
                throw new \Exception('Unexpected control character found');
            case JSON_ERROR_SYNTAX:
                throw new \Exception('Syntax error, malformed JSON');
            case JSON_ERROR_UTF8:
                throw new \Exception('Malformed UTF-8 characters, possibly incorrectly encoded');
            default:
                throw new \Exception('Unknown json error');
        }
    }
}
