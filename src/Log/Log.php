<?php
declare(strict_types=1);

namespace Nakima\Utils\Log;

class Log
{
    public static function trigger(string $message, int $level = E_USER_NOTICE): void
    {
        trigger_error($message, $level);
    }

    public static function triggerDeprecated(string $message): void
    {
        self::trigger($message, E_USER_DEPRECATED);
    }

    public static function triggerError(string $message): void
    {
        self::trigger($message, E_USER_ERROR);
    }

    public static function triggerNotice(string $message): void
    {
        self::trigger($message, E_USER_NOTICE);
    }

    public static function triggerWarning(string $message): void
    {
        self::trigger($message, E_USER_WARNING);
    }
}
