<?php
declare(strict_types=1);
namespace Nakima\Utils\Number;

/**
 * @author xgonzalez@nakima.es
 */

class Romanic
{
    public static function toRoman($number)
    {
        $table = array(
            'M' => 1000,
            'CM' => 900,
            'D' => 500,
            'CD' => 400,
            'C' => 100,
            'XC' => 90,
            'L' => 50,
            'XL' => 40,
            'X' => 10,
            'IX' => 9,
            'V' => 5,
            'IV' => 4,
            'I' => 1,
        );
        $return = '';

        while ($number > 0) {
            foreach ($table as $rom => $arb) {
                if ($number >= $arb) {
                    $number -= $arb;
                    $return .= $rom;
                    break;
                }
            }
        }

        return $return;
    }
}
