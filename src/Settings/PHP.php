<?php
declare(strict_types=1);
namespace Nakima\Utils\Settings;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

class PHP
{

    private static $maxFileSize = -1;

    // max upload file size in bytes
    public static function maxFileSize()
    {

        if (self::$maxFileSize < 0) {
            self::$maxFileSize = self::parseSize(ini_get('post_max_size'));
            $uploadMax = self::parseSize(ini_get('upload_max_filesize'));

            if ($uploadMax > 0 && $uploadMax < self::$maxFileSize) {
                self::$maxFileSize = $uploadMax;
            }
        }

        return self::$maxFileSize;
    }

    private static function parseSize($size)
    {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        } else {
            return round($size);
        }
    }
}
