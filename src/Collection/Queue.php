<?php
declare(strict_types=1);
namespace Nakima\Utils\Collection;

/**
 * @author xgonzalez@nakima.es
 */

class Queue extends \SplQueue
{

    private $object;

    public function pop()
    {
        $ret = parent::pop();
        if ($this->isEmpty()) {
            $this->object = null;
        } else {
            $this->object = $this->top();
        }

        return $ret;
    }

    public function push($value)
    {
        $this->object = $value;

        return parent::push($value);
    }

    public function peek()
    {
        return $this->object;
    }

    public function isEmpty()
    {
        return parent::isEmpty();
    }
}
