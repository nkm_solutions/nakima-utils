<?php
declare(strict_types=1);
namespace Nakima\Utils\File;

class File2
{

    public static function copy($src, $dst)
    {
        //$src = realpath($src);
        //$dst = realpath($dst);
        return copy($src, $dst);
    }

    public static function recurse_copy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src.'/'.$file)) {
                    recurse_copy($src.'/'.$file, $dst.'/'.$file);
                } else {
                    copy($src.'/'.$file, $dst.'/'.$file);
                }
            }
        }
        closedir($dir);
    }

    public static function encode($filename, $password = null, $password2 = null)
    {

        $resource = fopen($filename, "r");
        $content = fread($resource, filesize($filename));
        fclose($resource);

        if ($password2) {
            $content = self::parse($content, $password2);
        }
        if ($password) {
            $content = self::parse($content, $password);
        }

        $resource = fopen($filename, "w");
        fwrite($resource, $content, strlen($content));
        fclose($resource);

        return;

        // esto formaria parte del retorno y solo codigo de cliente
        $resource = fopen($filename, "w");
        fwrite($resource, base64_decode(explode(",", self::parse($content, $password))[1]), strlen($content));
        fclose($resource);

        return;
    }

    private static function parse($string, $key)
    {
        $text = $string;
        $outText = '';

        for ($i = 0; $i < strlen($text);) {
            for ($j = 0; $j < strlen($key); $j++, $i++) {
                if ($i < strlen($text)) {
                    $outText .= ($text[$i] ^ $key[$j]);
                }
            }
        }

        return $outText;
    }

    function strToHex($string)
    {
        $hex = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $hex .= dechex(ord($string[$i]));
        }

        return $hex;
    }

    function hexToStr($hex)
    {
        $string = '';
        for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
            $string .= chr(hexdec($hex[$i].$hex[$i + 1]));
        }

        return $string;
    }

    private static $b64Table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

    private static function b64Decode($data)
    {
        $o1;
        $o2;
        $o3;
        $h1;
        $h2;
        $h3;
        $h4;
        $bits;
        $i = 0;
        $result = "";
        if (!$data) {
            return $data;
        }
        $data .= "";
        do {
            $h1 = strpos(self::$b64Table, $data[$i]);
            $i++;
            $h2 = strpos(self::$b64Table, $data[$i]);
            $i++;
            $h3 = strpos(self::$b64Table, $data[$i]);
            $i++;
            $h4 = strpos(self::$b64Table, $data[$i]);
            $i++;

            $bits = $h1 << 18 | $h2 << 12 | $h3 << 6 | $h4;
            $o1 = $bits >> 16 & 0xff;
            $o2 = $bits >> 8 & 0xff;
            $o3 = $bits & 0xff;


            $result .= $o1;
            if ($h3 !== 64) {
                $result .= $o2;
                if ($h4 !== 64) {
                    $result .= $o3;
                }
            }
        } while ($i < strlen($data));
        var_dump($data);
        var_dum();

        return $result;
    }
}
