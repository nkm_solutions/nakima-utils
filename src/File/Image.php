<?php
declare(strict_types=1);
namespace Nakima\Utils\File;

/**
 * @author xgonzalez@nakima.es
 */

use Nakima\Utils\String\Text;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Image
{

    /**************************************************************************
     * Static Functions                                                       *
     **************************************************************************/

    public static function base64_to_UploadedFile($base64_image)
    {
        $data = explode(',', $base64_image);
        $mime_type = preg_replace("#^data:#", "", $data[0]);
        $mime_type = preg_replace("#;base64$#", "", $mime_type);
        $extension = preg_replace("#^image\/#", "", $mime_type);
        $fileName = Text::rstr(64);
        $path = tempnam(sys_get_temp_dir(), 'Image');

        $img_content = base64_decode($data[1]);

        $ifp = fopen($path, "wb");
        fwrite($ifp, $img_content);
        fclose($ifp);

        $file = new UploadedFile(
            $path, $fileName, $mime_type, null, null, true
        );

        return $file;
    }

    public static function createFromString($base64)
    {
        $img = imagecreatefromstring(base64_decode($base64));
        if (!$img) {
            return false;
        }

        $fileName = Text::rstr(64);
        $path = tempnam(sys_get_temp_dir(), 'Image');

        imagepng($img, $path);

        $info = getimagesize($path);
        unlink($path);

        if ($info[0] > 0 && $info[1] > 0 && $info['mime']) {
            if ($info['mime'] === "image/png") {
                $filename .= ".png";
                imagepng($img, tempnam(sys_get_temp_dir(), 'Image'));
            } elseif ($info['mime'] === "image/jpeg") {
                $filename .= ".jpg";
                imagejpeg($img, tempnam(sys_get_temp_dir(), 'Image'));
            } elseif ($info['mime'] === "image/gif") {
                $filename .= ".gif";
                imagegif($img, tempnam(sys_get_temp_dir(), 'Image'));
            }

            return new Image($img, tempnam(sys_get_temp_dir(), 'Image'));
        }

        return false;
    }

    /**************************************************************************
     * Class Functions                                                        *
     **************************************************************************/

    private $image;
    private $path;
    private $isImage;
    private $info;

    public function __construct($source, $path = "")
    {
        if ($source instanceof File) {
            $source = $source->getPathName();
            $this->path = $source;
            $this->image = imagecreatefromstring(file_get_contents($this->path));
        } else {
            if (is_string($source)) {
                $this->path = $source;
                $this->image = imagecreatefromstring(file_get_contents($this->path));
            } else {
                $this->path = $path;
                $this->image = $source;
            }
        }

        if (!$this->image) {
            $this->isImage = false;
        } else {
            $this->isImage = is_array(getimagesize($this->path));
        }


        if ($this->isImage) {
            $this->info = getimagesize($this->path);
        }

        if ($this->isGif() || $this->isPng()) {
            imagealphablending($this->image, false);
            imagesavealpha($this->image, true);
            //$transparent = imagecolorallocatealpha($this->image, 255, 255, 255, 127);
            //imagefilledrectangle($this->image, 0, 0, $this->info[0], $this->info[1], $transparent);
        }
    }

    private function _isImage($image)
    {
        if (!$this->image) {
            return false;
        } else {
            return @is_array(getimagesize($mediapath));
        }
    }

    public function isImage()
    {
        return $this->isImage;
    }

    public function getMimeType()
    {
        return $this->info["mime"];
    }

    public function isPng()
    {
        return $this->getMimeType() === "image/png";
    }

    public function isJpeg()
    {
        return $this->getMimeType() === "image/jpeg";
    }

    public function isGif()
    {
        return $this->getMimeType() === "image/gif";
    }

    public function transformToPng($destroy = false)
    {
        $path = tempnam(sys_get_temp_dir(), 'Image');

        imagepng($this->image, $path);

        if ($destroy) {
            $this->destroy();
        }

        return new Image($path);
    }

    public function getName()
    {
        return basename($this->path);
    }

    public function transformToJpeg($destroy = false, $color = array(255, 255, 255))
    {
        $path = tempnam(sys_get_temp_dir(), 'Image');

        if (!$this->isJpeg) {
            $bg = imagecreatetruecolor(imagesx($this->image), imagesy($this->image));
            imagefill($bg, 0, 0, imagecolorallocate($bg, $color[0], $color[1], $color[2]));
            imagealphablending($bg, true);
            imagecopy($bg, $this->image, 0, 0, 0, 0, imagesx($this->image), imagesy($this->image));
            $quality = 100;
            imagejpeg($bg, $path, $quality);
        }

        if ($destroy) {
            $this->destroy();
        }

        return new Image($path);
    }

    public function transformToGif($destroy = false)
    {
        $path = tempnam(sys_get_temp_dir(), 'Image');

        imagegif($this->image, $path);

        if ($destroy) {
            $this->destroy();
        }

        return new Image($path);
    }

    public function getSize()
    {
        return array(
            $this->info[0],
            $this->info[1],
        );
    }

    public function getFileSize()
    {
        return filesize($this->path);
    }

    public function resizeImage($width, $height, $destroy = false)
    {

        // TODO resize GIFs
        $extension = ".jpg";
        if ($this->isGif()) {
            $extension = ".gif";
        } else {
            if ($this->isPng()) {
                $extension = ".png";
            }
        }

        $name = Text::rstr(6).$extension;
        $path = tempnam(sys_get_temp_dir(), 'Image');

        $resized = imagecreatetruecolor($width, $height);

        if ($this->isGif() || $this->isPng()) {
            imagealphablending($resized, false);
            imagesavealpha($resized, true);
            $transparent = imagecolorallocatealpha($resized, 255, 255, 255, 127);
            imagefilledrectangle($resized, 0, 0, $width, $height, $transparent);
        }
        //imagecopyresized($resized, $this->image, 0, 0, 0, 0, $width, $height, $this->info[0], $this->info[1]);
        imagecopyresampled($resized, $this->image, 0, 0, 0, 0, $width, $height, $this->info[0], $this->info[1]);

        if ($this->isGif()) {
            $ret = imagegif($resized, $path);
        } else {
            if ($this->isPng()) {
                $ret = imagepng($resized, $path);
            } else {
                $ret = imagejpeg($resized, $path);
            }
        }

        if ($destroy) {
            $this->destroy();
        }

        if ($this->isGif()) {
            $gifDecoder = new GIFDecoder (fread(fopen($path, "rb"), filesize($path)));
            $delay = $gifDecoder->GIFGetDelays(); // get the delay between each frame
            var_dump(filesize($path));
            var_dump($path);
            var_dump($delay);
        }

        return new Image($resized, $path);
    }

    public function cropImage($x, $y, $offsetX, $offsetY, $destroy = false)
    {

        $extension = ".jpg";
        if ($this->isGif()) {
            $extension = ".gif";
        } else {
            if ($this->isPng()) {
                $extension = ".png";
            }
        }

        $name = Text::rstr(6).$extension;
        $path = tempnam(sys_get_temp_dir(), 'Image');

        $cropped = imagecreatetruecolor($x, $y);
        //  imagecopyresampled($dst_image, $src_image  , $dst_x, $dst_y, $src_x  , $src_y  , $dst_w, $dst_h, $src_w, $src_h);
        imagecopyresampled($cropped, $this->image, 0, 0, $offsetX, $offsetY, $x, $y, $x, $y);

        if ($this->isGif()) {
            $ret = imagegif($cropped, $path);
        } else {
            if ($this->isPng()) {
                $ret = imagepng($cropped, $path);
            } else {
                $ret = imagejpeg($cropped, $path);
            }
        }

        if ($destroy) {
            $this->destroy();
        }

        return new Image($cropped, $path);
    }

    public function fitInto($width, $height, $destroy = true)
    {
        $size = $this->getSize();

        $w = $size[0];
        $h = $size[1];

        if (($w / $width) > ($h / $height)) {
            $image = $this->resizeImage($height * $w / $h, $height, $destroy);
        } else {
            $image = $this->resizeImage($width, $width * $h / $w, $destroy);
        }

        $size = $image->getSize();

        $w = $size[0];
        $h = $size[1];

        $ox = abs(($w - $width) / 2);
        $oh = abs(($h - $height) / 2);

        $image = $image->cropImage($width, $height, $ox, $oh, $destroy);

        return $image;


    }

    public function destroy()
    {
        imagedestroy($this->image);
    }

    public function getFile()
    {
        return $this->image;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function upload($folder, $file, $destroy = false)
    {

        if ($this->isGif()) {
            $ret = imagegif($this->image, "$folder/$file");
        } else {
            if ($this->isPng()) {
                $ret = imagepng($this->image, "$folder/$file");
            } else {
                $ret = imagejpeg($this->image, "$folder/$file");
            }
        }

        if ($destroy) {
            $this->destroy();
        }


        return $ret;
    }

    public function getExtension()
    {
        if ($this->isPng()) {
            return "png";
        } else {
            if ($this->isGif()) {
                return "gif";
            } else {
                return "jpg";
            }
        }
    }

}
